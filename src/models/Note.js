const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: true,
  },
}, {
  timestamps: { createdAt: 'createdDate' },
  versionKey: false,
});

const Note = mongoose.model('notes', noteSchema);

module.exports = {
  Note,
};
