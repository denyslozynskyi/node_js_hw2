const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://Denys:very-secure-password@epamlabnodejs.krkbdg8.mongodb.net/notesApp?retryWrites=true&w=majority');

const { notesRouter } = require('./routers/notesRouter');
const { usersRouter } = require('./routers/usersRouter');
const { authRouter } = require('./routers/authRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/users/me', usersRouter);
app.use('/api/auth', authRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
