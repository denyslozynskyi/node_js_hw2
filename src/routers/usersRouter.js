const express = require('express');

const router = express.Router();
const { getUser, deleteUser, updateUserPassword } = require('../services/usersService');
const { authMiddleware } = require('../middleware/authoraizationMiddleware');

router.get('/', authMiddleware, getUser);
router.delete('/', authMiddleware, deleteUser);
router.patch('/', authMiddleware, updateUserPassword);

module.exports = {
  usersRouter: router,
};
