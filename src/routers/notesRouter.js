const express = require('express');

const router = express.Router();
const {
  createNote, getNotes, getNote, updateNote, toogleComletedField, deleteNote,
} = require('../services/notesServices');
const { authMiddleware } = require('../middleware/authoraizationMiddleware');

router.post('/', authMiddleware, createNote);
router.get('/', authMiddleware, getNotes);
router.get('/:id', authMiddleware, getNote);
router.put('/:id', authMiddleware, updateNote);
router.patch('/:id', authMiddleware, toogleComletedField);
router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
