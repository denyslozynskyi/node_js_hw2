const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/User');

// eslint-disable-next-line consistent-return
async function registerUser(req, res, next) {
  const { username, password } = req.body;

  if (!username) {
    return res.status(400).json({ message: 'Please, specify username field' });
  }

  if (!password) {
    return res.status(400).json({ message: 'Please, specify password field' });
  }

  const user = new User({
    username,
    password: await bcryptjs.hash(password, 10),
  });

  user.save()
    .then(() => res.status(200).json({ message: 'Success' }))
    .catch((err) => {
      next(err);
      console.log(err);
    });
}

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    // eslint-disable-next-line no-underscore-dangle
    const payload = { username: user.username, name: user.name, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.status(200).json({
      message: 'Success',
      jwt_token: jwtToken,
    });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};
