/* eslint-disable no-trailing-spaces */
const bcryptjs = require('bcryptjs');
const { User } = require('../models/User');

function getUser(req, res) {
  return User.find({ _id: req.user.userId })
    .then((result) => {
      const { _id, username, createdDate } = result[0];
      
      res.status(200).json({
        user: {
          _id,
          username,
          createdDate,
        },
      });
    });
}

function deleteUser(req, res) {
  return User.findByIdAndDelete({ _id: req.user.userId })
    .then(() => {
      res.status(200).json({ message: 'Success' });
    });
}

async function updateUserPassword(req, res) { 
  const newPassword = await bcryptjs.hash(req.body.newPassword, 10);

  if (req.body.oldPassword === req.body.newPassword) {
    return res.status(400).json({ message: 'New password can not be equal to pld password' });
  }

  return User.findByIdAndUpdate({ _id: req.user.userId }, { $set: { password: newPassword } })
    .then(() => {
      res.status(200).json({ message: 'Success' });
    });
}

module.exports = {
  getUser,
  deleteUser,
  updateUserPassword,
};
