/* eslint-disable no-trailing-spaces */
const { Note } = require('../models/Note');

// eslint-disable-next-line consistent-return
function createNote(req, res) {
  const { text } = req.body;
  const { userId } = req.user;
  
  if (!text) {
    return res.status(400).json({ message: 'Please, specify text parameter' });
  }

  const note = new Note({
    text,
    userId,
  });

  note.save().then(() => {
    res.status(200).json({ message: 'Success' });
  });
}

function getNotes(req, res) {
  return Note.find({ userId: req.user.userId })
    .then((notes) => {
      const offset = req.query.offset ? req.query.offset : 0;
      const limit = req.query.limit ? req.query.limit : notes.length;
      const count = notes.length;     
      const result = notes.length > 0 ? notes.splice(+offset, +limit) : [];

      res.status(200).json({
        offset: +offset,
        limit: +limit,
        count,
        notes: result,
      });
    });
}

async function getNote(req, res) {
  const note = await Note.find({ _id: req.params.id, userId: req.user.userId });
  
  if (note.length > 0) {
    return Note.find({ _id: req.params.id, userId: req.user.userId })
      .then((result) => {
        res.status(200).json({
          note: result[0],
        });
      });
  }

  return res.status(400).json({ message: 'No notes with this id in your notes' });
}

async function updateNote(req, res) {
  const { text } = req.body;
  const note = await Note.find({ _id: req.params.id, userId: req.user.userId });

  if (note[0].text === text) {
    return res.status(400).json({ message: 'Old text in note equal to  new text' });
  }

  if (!text) {
    return res.status(400).json({ message: 'Please, specify text parameter' });
  }

  if (note.length > 0) {
    // eslint-disable-next-line max-len
    return Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { text } })
      .then(() => {
        res.status(200).json({ message: 'Success' });
      });
  }

  return res.status(400).json({ message: 'No notes with this id in your notes' });
}

async function toogleComletedField(req, res) {
  const note = await Note.find({ _id: req.params.id, userId: req.user.userId });

  if (note.length > 0) {
    const { completed } = note[0];
    // eslint-disable-next-line max-len
    return Note.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { completed: !completed } })
      .then(() => {
        res.status(200).json({ message: 'Success' });
      });
  }
  return res.status(400).json({ message: 'No notes with this id in your notes' });
}

async function deleteNote(req, res) {
  const note = await Note.find({ _id: req.params.id, userId: req.user.userId });
  if (note.length > 0) {
    return Note.findByIdAndDelete({ _id: req.params.id, userId: req.user.userId })
      .then(() => {
        res.status(200).json({ message: 'Success' });
      });
  }
  return res.status(400).json({ message: 'No notes with this id in your notes' });
}

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  toogleComletedField,
  deleteNote,
};
